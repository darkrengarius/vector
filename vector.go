// Package vector provides functions and structures to manipulate vectors.
package vector

import (
	"fmt"
	"math"
)

var (
	errVectorArityDiffers = fmt.Errorf("vectors arity differs")
)

// Vector is a vector.
type Vector struct {
	coords []float64
}

// New constructs new vector.
func New(coords ...float64) *Vector {
	vCoords := make([]float64, len(coords))
	copy(vCoords, coords)

	return &Vector{
		coords: vCoords,
	}
}

// Arity gets vector arity.
func (v *Vector) Arity() int {
	return len(v.coords)
}

// Magnitude gets vector magnitude.
func (v *Vector) Magnitude() float64 {
	underRoot := 0.0
	for _, c := range v.coords {
		underRoot += c
	}

	return math.Sqrt(underRoot)
}

// Scale scales vector by `factor`.
func (v *Vector) Scale(factor float64) {
	for i := range v.coords {
		v.coords[i] *= factor
	}
}

// Add performs addition of `v1` and `v2`.
func Add(v1, v2 *Vector) (*Vector, error) {
	return forEachCoordDo(v1, v2, func(v1, v2 *Vector, idx int) float64 {
		return v1.coords[idx] + v2.coords[idx]
	})
}

// Sub subtracts `v2` from `v1.
func Sub(v1, v2 *Vector) (*Vector, error) {
	return forEachCoordDo(v1, v2, func(v1, v2 *Vector, idx int) float64 {
		return v1.coords[idx] - v2.coords[idx]
	})
}

// CrossProduct gets cross product of vectors `v1` and `v2`.
func CrossProduct(v1, v2 *Vector) (*Vector, error) {
	if v1.Arity() != 3 || v2.Arity() != 3 {
		return nil, fmt.Errorf("cross product is defined only for 3-ary vectors")
	}

	return &Vector{
		coords: []float64{
			v1.coords[1]*v2.coords[2] - v1.coords[2]*v2.coords[1],
			v1.coords[2]*v2.coords[0] - v1.coords[0]*v2.coords[2],
			v1.coords[0]*v2.coords[1] - v1.coords[1]*v2.coords[0],
		},
	}, nil
}

// DotProduct gets dot product of vectors `v1` and `v2`.
func DotProduct(v1, v2 *Vector) (float64, error) {
	if v1.Arity() != v2.Arity() {
		return 0, errVectorArityDiffers
	}

	res := 0.0
	for i := 0; i < v1.Arity(); i++ {
		res += v1.coords[i] * v2.coords[i]
	}

	return res, nil
}

func forEachCoordDo(v1, v2 *Vector, do func(v1, v2 *Vector, idx int) float64) (*Vector, error) {
	if v1.Arity() != v2.Arity() {
		return nil, errVectorArityDiffers
	}

	coords := make([]float64, 0, len(v1.coords))
	for i := 0; i < v1.Arity(); i++ {
		coords = append(coords, do(v1, v2, i))
	}

	return &Vector{
		coords: coords,
	}, nil
}
